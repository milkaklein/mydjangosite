from django.db import models


class Suppliers(models.Model):
    supplierNmae = models.CharField(max_length=20)
    site = models.URLField(max_length=100)
    phone=models.CharField(max_length=15)

    def __str__(self):
        return self.supplierNmae


class Products(models.Model):
    product = models.CharField(max_length=100)
    makat = models.CharField(max_length=20)
    ourPrice = models.IntegerField(default=0)

    def __str__(self):
        return self.product

class ProductBySupplier(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    supplier = models.CharField(max_length=20)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.product+" "+self.supplier